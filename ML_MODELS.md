# Comparing Existing ML Models for HAR

## [Deep Learning for Sensor-Based Human Activity Recognition](https://becominghuman.ai/deep-learning-for-sensor-based-human-activity-recognition-970ff47c6b6b) (from project proposal)

Uses 2.7GB [_Heterogeneity Activity Recognition Data Set_](https://archive.ics.uci.edu/ml/datasets/Heterogeneity+Activity+Recognition)
* Activities: 'Biking', 'Sitting', 'Standing', 'Walking', 'Stair Up' and 'Stair down'
* Sensors: Two embedded sensors, i.e., __Accelerometer__ and __Gyroscope__, sampled at the highest frequency the respective device allows
* Data set seperated into training and evaluation folders with files for phone accel and gyro and watch accel and gyro
* Devices: 4 smartwatches (2 LG watches, 2 Samsung Galaxy Gears)
* 8 smartphones (2 Samsung Galaxy S3 mini, 2 Samsung Galaxy S3, 2 LG Nexus 4, 2 Samsung Galaxy S+)
* Recordings: 9 users
* Attributes: 'Index', 'Arrival_Time', 'Creation_Time', 'x', 'y', 'z', 'User', 'Model', 'Device', 'gt'
* No mentions of collection methods (hip-mounted, pocket etc)

Combination of a __CNN__ (Convolutional Neural Network) and a __RNN__ (Recurrent Neural Network)

Split into series of data intervals along time
* CNN __extracts local features__
* Intervals fed into a CNN to learn __intra__-interval local interactions
* RNN __extracts temporal dependencies__
* Intra-interval representations are fed into a RNN to learn the __inter__-interval relationships

The model can be "freezed" and exported as a ".pb" file and used on Android
> 1. Freeze the model and export it as a file (model.pb for instance)
> 2. Add Google inference library for Android in the Android app
> 3. Create an Activity in the Android App that feeds the model each 50 Hz (Sample rate)
with sensors real-time accelerometer and gyroscope data
> 4. Return the predicted activity or the activities with their prediction confidence (probability)

Achieved __92%__ testing accuracy

Built with __TensorFlow 1.1__ and based on the _Deep Sense_ model.

#### Pros:
* Based on _Deep Sense_

#### Cons:
* No code provided

#### Resources
[DeepSense: A Unified Deep Learning Framework for Time-Series Mobile Sensing Data Processing](https://arxiv.org/abs/1611.01942)

[Guillaume Chevalier, LSTMs for Human Activity Recognition, 2016](https://github.com/guillaume-chevalier/LSTM-Human-Activity-Recognition)

## [Deep Sense](https://arxiv.org/abs/1611.01942)

Uses the _Heterogeneity Activity Recognition Data Set_ (same as above)
* Data collected "while carrying smartwatches and smartphones".

Combination of __CNN__ and __RNN__

Three convolutional layers per sensor which are concatenated and three convolutional layers after concatenation. 64 filters in each convolutional layer, ReLU as activation, 5 second samples

Stacked __GRU__ (gated recurrent unit which is similar to LSTMs) for recurrent layers


Full model found on [GitHub](https://github.com/yscacaca/DeepSense). Includes Android conversion.

Achieved __~94%__ accuracy

Model uses __TensorFlow 1.1__ and has some deprecated code in data ingestion pipeline.

#### Pros:
* Most complete code provided including Android conversion
* Trained on a large data set

#### Cons:
* Some deprecated code and errors. Wouldn't compile straightaway

## [LSTMs for Human Activity Recognition](https://github.com/guillaume-chevalier/LSTM-Human-Activity-Recognition/blob/master/LSTM.ipynb)


Uses 280MB [_Human Activity Recognition Using Smartphones Data Set_](https://archive.ics.uci.edu/ml/datasets/human+activity+recognition+using+smartphones)
* Activities: WALKING, WALKING_UPSTAIRS, WALKING_DOWNSTAIRS, SITTING, STANDING, LAYING.
* Collected by mounting on hip. Might be a problem if the objective was to be able to predict by having it in pocket.
* Some sort of filtering and processing has been applied. Same processing would have to be implemented on the mobile side.
>The sensor signals (accelerometer and gyroscope) were pre-processed by applying noise filters and then sampled in fixed-width sliding windows of 2.56 sec and 50% overlap (128 readings/window). The sensor acceleration signal, which has gravitational and body motion components, was separated using a Butterworth low-pass filter into body acceleration and gravity. The gravitational force is assumed to have only low frequency components, therefore a filter with 0.3 Hz cutoff frequency was used. From each window, a vector of features was obtained by calculating variables from the time and frequency domain.

Two layer stacked LSTM

Achieved __91%__ on the test data set

Built with __Tensorflow 1.0__

#### Pros:
* Runnable code provided

#### Cons:
* Data gathered while phone hip mounted
* Same pre-processing methods would need to be implemented on mobile

## [HAR Stacked Residual Bi-Directional LSTM](https://github.com/guillaume-chevalier/HAR-stacked-residual-bidir-LSTMs)

Uses the same data set as above

Achieved __94%__ accuracy on same data set

Much more complex. Too large of a model drains the phone battery quickly. However, in a real world setting a larger model might be useful

#### Pros:
* Might generalize better if trained on larger data set

#### Cons:
* Same data set as above
* Possibly heavy to run on mobile
* Marginal increase in accuracy compared to the simpler model


# Conclusion

Based on the pros and cons of each model, for now it seems as though the **_Deep Sense_** model would be the best choice. Some debugging still needed to get it to run though.

Once the model has been trained, the data ingestion pipeline needs to be revamped so that data from the database can be fed into the model.
