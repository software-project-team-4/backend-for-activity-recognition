# Contributing Rules

## Commit messages
Communicate context about a change to fellow developers
* Separate subject from body with a blank line
* Limit the subject line to 50 characters
* Capitalize the subject line
* Do not end the subject line with a period
* Use the imperative mood in the subject line
* Wrap the body at 72 characters
>**Note** For small changes body not necessarily needed
* Use the body to explain what and why vs. how
