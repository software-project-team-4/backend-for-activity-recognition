# Backend for Human Activity Recognition on Mobile

### Description

Server is meant to collect sensor data for re-training the ML model and monitor data and users with Grafana.

**Architecture:**\
[Database Schema](https://gitlab.com/software-project-team-4/backend-for-activity-recognition/blob/feature_documentation/Database_Schema.png)\
[Backend Architecture](https://gitlab.com/software-project-team-4/backend-for-activity-recognition/blob/feature_documentation/Backend_Architecture__2_.png)

## Run server locally

- Install [SBT](https://www.scala-sbt.org/)
- Install [Postgres](https://www.postgresql.org/download/) and create a database
  > **Default settings are**\
  > databaseName = "sensordb"\
  > user = "postgres"\
  > password = "1"\
  > However can be changed from _application.conf_ in _conf_ folder
- Pull git repo
- Cd into project folder
- Type `sbt run`

## Monitor data using Grafana

- Install and start [Grafana](https://grafana.com/docs/installation/)
- Add your database to Data Source
- Import new dashboard (import file is in grafana folder in the repo)


## Run tests

- Cd into project folder
- To run all tests type `sbt test` and press enter
- To only run one test `sbt "test:testOnly *<TestClassName>"`

## Deploy server to EC2:

- Pull git repo
- Type `sbt` in project folder
- Type `dist` in sbt console (It creates binary zip from the project)
- Send zip to EC2 (using scp) `scp -i <path to secret key> <path to file> ec2-user@<Instace's public DNS>:/<path to location in ec2>`

### In EC2:
- Unzip the binary.
- You have to change one file to executable `chmod +x scala-backend-<version>-SNAPSHOT/bin/scala-backend`
- Then start the server `./scala-backend-<version>-SNAPSHOT/bin/scala-backend -Dconfig.file=<absolute path>scala-backend-<version>-SNAPSHOT/conf/aws.conf &`

### To start Ml model:
- Move model file to EC2
- pull tensorflow serving docker image `docker pull tensorflow/serving`
- Type `docker run -t --rm -p 8501:8501 -v <absolute path to models folder>models:/models -e MODEL_NAME=sensorflow -t tensorflow/serving &`

### Setting Grafana Data Source:
- Open SSH tunnel `ssh -L <Local port>:<RDS endpoint><RDS port> ec2-user@<EC2 public ip> -i <path to secret key>`
    >**Data Source**\
    >Host: localhost:Local Port\
    >Database: RDS db instance name\
    >User: Master username\
    >Password: RDS db password\
    >SSL mode: Disabled\
Import new dashboard (import file is in grafana folder in the repo)





