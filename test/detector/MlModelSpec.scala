package ml
import mockws.MockWS
import mockws.MockWSHelpers._

import scala.util.Random
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.Injecting

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import controllers.Assets._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import database.Database
import play.api.{Application, Configuration, Logger}
import play.api.libs.ws._
import play.api.mvc.Result
import database.{Database, Validate}
import database.Entities._
import detector.Models
import utils.{DataGenerator, TestDatabase}
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.json.{JsError, JsObject, JsSuccess, JsValue, Json}
import play.api.inject.bind

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.test.Helpers._

class MlModelTest extends PlaySpec with GuiceOneAppPerSuite with Injecting {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .build()

  def await[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  val db = app.injector.instanceOf[Database]
  val config = app.injector.instanceOf[Configuration]
  val modelName = "sensorflow_3.1"
  val url = Models.urls(modelName)

  val ws = MockWS {
    case (POST, url) =>
      Action { request =>
        val json = (request.body.asJson.get \ "instances")
        val features = json.validate[Seq[Seq[Seq[Double]]]]

        features match {

          case s: JsSuccess[Seq[Seq[Seq[Double]]]] =>
            if (s.get.forall(f => f.size == Models.inputLengths(modelName))) {

              val predictions =
                DataGenerator.predictions(s.get.size,
                                          Models.outputLengths(modelName))

              Ok(predictions)
            } else {
              BadRequest("Input is wrong shape")
            }

          case e: JsError =>
            BadRequest("Json validation failed")
        }
      }
  }

  "Test prediction" should {
    await(db.probabilities.deleteAll())
    await(db.eventTypes.deleteAll())

    val model = new MlModel(config, db, ws, modelName)
    val user = DataGenerator.users(1).head
    await(db.users.insert(user))

    "insert event into the database" in {
      model.predict(user.userId,
                    DataGenerator.inputCompDatapoints(250))
      Thread.sleep(1000)
      await(db.events.all()).size mustBe 1
      await(db.probabilities.all()).size mustBe model.outputLength

      //Clear the database
      await(db.probabilities.deleteAll())
      await(db.events.deleteAll())

    }

    "insert 1 event when receives 300 datapoints" in {
      model.predict(user.userId,
                    DataGenerator.inputCompDatapoints(300))
      Thread.sleep(1000)
      await(db.events.all()).size mustBe 1
      await(db.probabilities.all()).size mustBe model.outputLength

      //Clear the database
      await(db.probabilities.deleteAll())
      await(db.events.deleteAll())

    }

    "insert 2 event when receives 500 datapoints" in {
      model.predict(user.userId,
                    DataGenerator.inputCompDatapoints(500))
      Thread.sleep(1000)
      await(db.events.all()).size mustBe 2
      await(db.probabilities.all()).size mustBe model.outputLength*2

      //Clear the database
      await(db.probabilities.deleteAll())
      await(db.events.deleteAll())
      await(db.eventTypes.deleteAll())
      await(db.users.deleteAll())

    }
  }

}
