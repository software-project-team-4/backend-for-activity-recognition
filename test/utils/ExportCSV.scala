package utils

import java.io.{File, PrintWriter}

import database.Database
import database.Entities.{Annotation, RawData}
import org.scalatest.{FlatSpec, Ignore, TestData}
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Injecting

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Exports all datapoints with annotations in database to a csv file
  *
  * Remove the @Ignore annotation to run
  */
@Ignore
class ExportCSV extends FlatSpec with GuiceOneAppPerTest with Injecting {

  override def newAppForTest(testData: TestData): Application = {
    new GuiceApplicationBuilder()
      .overrides(bind[Database].to[LocalDatabase])
      .build()
  }

  val title: String =
    "timestamp,accelX,accelY,accelZ,gyroX,gyroY,gyroZ,user,activity\n"

  //Executes the export function if the test isn't ignored
  "export all" should "export all" in {
    export()
  }

  /**
    * Check whether or not an annotation annotates a given datapoint
    *
    * @param data       the datapoint
    * @param annotation the annotation
    */
  def isAnnotatedBy(data: RawData, annotation: Annotation): Boolean = {
    data.timestamp >= annotation.startTimestamp && data.timestamp <= annotation.endTimestamp
  }

  /**
    * Format a line of annotated data to csv format
    *
    * @param data       the data
    * @param annotation the annotation
    */
  def formatLine(data: RawData, annotation: Annotation): String = {
    s"${data.timestamp},${data.accX},${data.accY},${data.accZ},${data.gyrX},${data.gyrY},${data.gyrZ}," +
      s"${annotation.userId},${annotation.eventName}"
  }

  /**
    * Export data
    */
  def export(): Unit = {

    val database: Database = app.injector.instanceOf[Database]

    val data =
      Await.result(database.compData.all(), Duration.Inf).groupBy(_.userId)

    val writer = new PrintWriter(new File("export.csv"))
    writer.write(title)
    writer.write(
      Await
        .result(database.annotations.all(), Duration.Inf)
        .groupBy(_.userId)
        .flatMap({
          case (userId, annotations) =>
            annotations.flatMap(annotation => {
              data(userId)
                .filter(isAnnotatedBy(_, annotation))
                .map(formatLine(_, annotation))
            })
        })
        .mkString("\n"))

    writer.close()
  }

}
