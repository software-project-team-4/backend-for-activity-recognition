package utils

import authentication.Authentication
import database.Database
import database.Entities.User

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * An authentication class to be used for testing
  *
  * Doesn't verify the token but instead uses it as the userID and generates name, last name and email
  * from it by appending the type of the column respectively
  *
  * @see [[https://www.playframework.com/documentation/2.6.x/ScalaTestingWithGuice#overriding-bindings-in-a-functional-test]] for usage example
  */
class TestAuthenticator extends Authentication {

  /**
    * Adds a new user if the user doesn't exist yet
    *
    * @param tokenString the GoogleIdTokenString sent by the user
    * @return userId
    */
  override def checkUser(tokenString: String)(
      implicit database: Database): Option[String] = {
    if (tokenString.isEmpty) {
      None
    } else {
      import database.dbProfile.api._
      val user =
        Await.result(database.users.filter(_.userId === tokenString),
                     Duration.Inf)
      if (user.isEmpty) {
        Await.ready(database.users.insert(
                      User(tokenString,
                           s"${tokenString}_name",
                           s"${tokenString}_lastName",
                           s"${tokenString}_email")),
                    Duration.Inf)
      }
      Some(tokenString)
    }
  }
}
