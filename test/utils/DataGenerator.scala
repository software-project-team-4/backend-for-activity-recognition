package utils

import java.sql.Timestamp

import database.Validate.{AnnotationData, Datapoint, EventData, RawDatapoint}
import database.{Entities, Validate}
import play.api.libs.json._
import play.api.libs.ws.WSResponse
import play.api.test.WsTestClient

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

/**
  * Contains utility methods for generating random data
  */
object DataGenerator {

  /**
    * The maximum time difference from current time when generating random timestamps
    */
  val maximumTimeDifference = 100000

  /** Generates an array of new users
    *
    * @param nofUsers number of users to be generated
    * @return an array of User objects
    */
  def users(nofUsers: Int): Array[Entities.User] = {
    (0 until nofUsers).map { id =>
      Entities.User(s"user$id", s"firstname$id", s"lastname$id", s"email$id")
    }.toArray
  }

  /**
    * Generates a random seq of datapoints in the input format
    * @param n number of datapoints to be generated
    * @return a sequence of [[database.Validate.Datapoint]]s
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  def inputDatapoints(n: Int): Seq[Datapoint] = {
    Seq.fill[Datapoint](n)(Datapoint(randTime, randData, randData, randData))
  }

  def inputCompDatapoints(n: Int): Seq[RawDatapoint] = {
    Seq.fill[RawDatapoint](n)(RawDatapoint(randTime, randData, randData, randData, randData, randData, randData))
  }

  /**
    * Generates a random seq of eventData in the input format
    *
    * @param n number of datapoints to be generated
    * @return a sequence of [[database.Validate.AnnotationData]]s
    */
  def inputAnnotationData(n: Int): Seq[AnnotationData] = {
    Seq.fill[AnnotationData](n)(
      AnnotationData(randTime, randTime, "TEST_Event"))
  }

  /**
    * Generates a random seq of eventData in the input format
    *
    * @param n number of datapoints to be generated
    * @return a sequence of [[database.Validate.AnnotationData]]s
    */
  def inputEventData(n: Int, probLength: Int): Seq[EventData] = {
    Seq.fill[EventData](n)(
      EventData(randTime,
                randTime,
        Seq.fill[Double](probLength)(Random.nextDouble()).mkString("[[", "," ,"]]"),
                "sensorflow_1.2"))
  }

  /** Generates random datapoints for the given list of users
    *
    * @param users         a list of User objects
    * @param nofDatapoints number of datapoints to be generated in total
    * @return a list of SensorDatapoint objects
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  def datapoints(users: Seq[Entities.User],
                 nofDatapoints: Int): Seq[Entities.Accel] = {
    Seq
      .fill[Int](nofDatapoints)(Random.nextInt(users.size))
      .map(id =>
        Entities
          .Accel(users(id).userId, randTime, randData, randData, randData))
  }

  @deprecated("Old", "1.1-SNAPSHOT")
  def datapointsJson(users: Seq[Entities.User], nofDatapoints: Int): JsValue = {
    val data = datapoints(users, nofDatapoints)
    Validate.toJson(data)
  }

  def compDatapoints(users: Seq[Entities.User],
                 nofDatapoints: Int): Seq[Entities.RawData] = {
    Seq
      .fill[Int](nofDatapoints)(Random.nextInt(users.size))
      .map(id =>
        Entities
          .RawData(users(id).userId, randTime, randData, randData, randData, randData, randData, randData))
  }

  def compDatapointsJson(users: Seq[Entities.User], nofDatapoints: Int): JsValue = {
    val data = compDatapoints(users, nofDatapoints)
    Validate.toJsonComp(data)
  }

  def predictions(batchSize: Int, outputLength: Int): JsObject = {
    Json.obj(
      "predictions" -> Json.toJson(
        Seq.fill[Seq[Double]](batchSize)(Seq.fill[Double](outputLength)(0))
      )
    )
  }

  def randTime: Long = {
    System.currentTimeMillis - Random.nextInt(maximumTimeDifference)
  }

  def randData: Double = Random.nextDouble * 20 - 10

  /** Creates a Seq of FakePhones for simulating concurrent users
    *
    * @param users                a Seq of User objects
    * @param delayMillis          the range for random time intervals for delays
    * @param nofDatapointsPerSend datapoints sent per packet
    * @param nofSends             number of sends simulated in total per user
    */
  def concurrentUserTest(
      users: Seq[Entities.User],
      delayMillis: Int,
      nofDatapointsPerSend: Int,
      nofSends: Int
  ): Future[Seq[WSResponse]] = {

    val phones: Seq[FakePhone] = users.map(user => new FakePhone(user))
    val futures =
      phones.map(phone => Future(phone.send(delayMillis, nofDatapointsPerSend)))
    Future.sequence(futures)
  }

  class FakePhone(user: Entities.User) {
    def timestamp: Timestamp = new Timestamp(System.currentTimeMillis)

    def randData: Double = Random.nextDouble * 20 - 10

    var totalDatapointsSent = 0
    var totalPacketsSent = 0
    val url = "http://localhost:9000/insertData"

    def jsonData(nofDatapoints: Int): JsValue = {
      val datapoints =
        Seq
          .fill[Entities.User](nofDatapoints)(user)
          .map(
            user =>
              Entities.RawData(user.userId,
                             timestamp.getTime,
                             randData,
                             randData,
                             randData,
                             randData,
                             randData,
                             randData))

      totalDatapointsSent += nofDatapoints
      totalPacketsSent += 1

      Validate.toJsonComp(datapoints)
    }

    def send(delayMillis: Int, nofDatapointsPerSend: Int): WSResponse = {
      Thread.sleep(Random.nextInt(delayMillis))
      val response = WsTestClient.withClient {
        _.url(url).post(jsonData(nofDatapointsPerSend))
      }
      Await.result(response, Duration.Inf)
    }

  }
}
