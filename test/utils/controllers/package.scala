package utils

import database.Validate.DataIn
import play.api.libs.json.{Json, OFormat}

package object controllers {

  case class TestJson(token: String, data: String) extends DataIn()

  implicit val testJsonFormat: OFormat[TestJson] = Json.format[TestJson]
}
