package utils.controllers

import authentication.Authentication
import com.google.inject.Inject
import controllers.DataController
import database.Database
import play.api.mvc.ControllerComponents

class TestController @Inject()(implicit db: Database,
                               authentication: Authentication,
                               cc: ControllerComponents)
    extends DataController[TestJson]() {

  override def handleInsert(userId: String, data: TestJson): Unit = {}
}
