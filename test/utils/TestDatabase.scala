package utils

import database.Database
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import play.db.NamedDatabase

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * A test database class to be used for testing
  *
  * @see [[https://www.playframework.com/documentation/2.6.x/ScalaTestingWithGuice#overriding-bindings-in-a-functional-test] for usage example]]
  */
class TestDatabase @Inject()(
    @NamedDatabase("test") dbConfigProvider: DatabaseConfigProvider)
    extends Database(dbConfigProvider)
