package utils

import database.Database
import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import play.db.NamedDatabase
import scala.concurrent.ExecutionContext.Implicits.global

class LocalDatabase @Inject()(
    @NamedDatabase("default") dbConfigProvider: DatabaseConfigProvider)
    extends Database(dbConfigProvider) {}
