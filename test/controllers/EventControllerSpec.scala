package controllers

import authentication.Authentication
import database.{Database, Validate}
import org.scalatest.concurrent.TimeLimits._
import org.scalatest.time.SpanSugar._
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._
import utils.controllers.{TestController, TestJson}
import utils.{DataGenerator, TestAuthenticator, TestDatabase}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class EventControllerSpec
    extends PlaySpec
    with GuiceOneAppPerSuite
    with Injecting {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .overrides(bind[Authentication].to[TestAuthenticator])
    .overrides(bind[DataController[TestJson]].to[TestController])
    .build()

  "EventController.insertData" should {

    "insert event data to the database" in {
      val controller = app.injector.instanceOf[EventController]
      val database = app.injector.instanceOf[Database]

      /**
        * Length of the probability array
        */
      val probLength = 6

      val eventData = DataGenerator.inputEventData(1, probLength)

      val result = controller.insertData()(
        FakeRequest(POST, "/").withJsonBody(
          Json.toJson(Validate.DataInEvent(eventData, "insertTest"))
        )
      )

      status(result) mustBe OK

      //database should have the user now
      import database.dbProfile.api._
      Await
        .result(
          database.users.filter(_.userId === "insertTest"),
          Duration.Inf
        )
        .size mustBe 1
      Await
        .result(
          database.events.all(),
          Duration.Inf
        )
        .size mustBe 1
      //The data should be there aswell
      failAfter(1000 millis){
        var done: Boolean = false
        while (!done) {
          if (Await
            .result(
              database.probabilities.all(),
              Duration.Inf
            )
            .size == 1 * probLength) {
            done = true
          }
        }
      }
    }
  }
}
