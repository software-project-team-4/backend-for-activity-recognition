package controllers

import authentication.Authentication
import database.{Database, Validate}
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._
import utils.controllers.{TestController, TestJson}
import utils.{DataGenerator, TestAuthenticator, TestDatabase}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class AnnotationControllerSpec
    extends PlaySpec
    with GuiceOneAppPerSuite
    with Injecting {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .overrides(bind[Authentication].to[TestAuthenticator])
    .overrides(bind[DataController[TestJson]].to[TestController])
    .build()

  "AnnotationController.insertData" should {
    "accept a valid json with empty data lists" in {
      val controller = app.injector.instanceOf[AnnotationController]
      val insert = controller
        .insertData()
        .apply(FakeRequest(POST, "/").withJsonBody(
          Json.toJson(Validate.DataInEventAnnotation(Seq(), "testUser"))))
      status(insert) mustBe OK
    }
    "insert annotation data to the database" in {
      val controller = app.injector.instanceOf[AnnotationController]

      val numberOfDatapoints = 10

      val annotationData = DataGenerator.inputAnnotationData(numberOfDatapoints)

      val result = controller.insertData()(
        FakeRequest(POST, "/").withJsonBody(
          Json.toJson(
            Validate.DataInEventAnnotation(annotationData, "insertTest"))
        )
      )

      val database = app.injector.instanceOf[Database]

      //database should have the user now
      import database.dbProfile.api._
      Await
        .result(
          database.users.filter(_.userId === "insertTest"),
          Duration.Inf
        )
        .size mustBe 1

      //The data should be there aswell
      Await
        .result(
          database.annotations.all(),
          Duration.Inf
        )
        .size mustBe numberOfDatapoints
    }
  }
}
