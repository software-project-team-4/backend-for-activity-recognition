package controllers

import authentication.Authentication
import database.{Database, Validate}
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.{FakeRequest, Injecting}
import play.api.test.Helpers.POST
import utils.controllers.{TestController, TestJson}
import utils.{DataGenerator, TestAuthenticator, TestDatabase}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@deprecated("Old", "1.1-SNAPSHOT")
class RawDataControllerSpec
    extends PlaySpec
    with GuiceOneAppPerSuite
    with Injecting {
  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .overrides(bind[Authentication].to[TestAuthenticator])
    .overrides(bind[DataController[TestJson]].to[TestController])
    .build()

  "RawDataController.insertData" should {
    "insert data to the database" ignore {
      val controller = app.injector.instanceOf[RawDataController]

      val numberOfDatapoints = 10

      val accelData = DataGenerator.inputDatapoints(numberOfDatapoints)
      val gyroData = DataGenerator.inputDatapoints(numberOfDatapoints)

      val result = controller.insertData()(
        FakeRequest(POST, "/").withJsonBody(
          Json.toJson(Validate.RawDataIn(accelData, gyroData, "insertTest"))
        )
      )

      val database = app.injector.instanceOf[Database]

      //database should have the user now
      import database.dbProfile.api._
      Await
        .result(
          database.users.filter(_.userId === "insertTest"),
          Duration.Inf
        )
        .size mustBe 1

      //The data should be there aswell
      Await
        .result(
          database.accels.all(),
          Duration.Inf
        )
        .size mustBe numberOfDatapoints
      Await
        .result(
          database.gyro.all(),
          Duration.Inf
        )
        .size mustBe numberOfDatapoints
    }
  }

}
