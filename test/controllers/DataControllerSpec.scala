package controllers

import authentication.Authentication
import database.Validate.EventData
import database.{Database, Validate}
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._
import utils.controllers.{TestController, TestJson, testJsonFormat}
import utils.{DataGenerator, TestAuthenticator, TestDatabase}

class DataControllerSpec
    extends PlaySpec
    with GuiceOneAppPerSuite
    with Injecting {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .overrides(bind[Authentication].to[TestAuthenticator])
    .overrides(bind[DataController[TestJson]].to[TestController])
    .build()

  def invalidJson(): Seq[String] = {
    Seq(
      //Missing token
      """
          {          
            "data" : ""
          }
        """,
      //Missing everything
      """
        {
        
        }
      """,
      //Missing data
      """
        {
          "token" : "testUser"
        }
      """
    )
  }

  "DataController POST" should {
    "accept a valid json with empty data" in {
      val controller = app.injector.instanceOf[TestController]
      val insert = controller
        .insertData()
        .apply(FakeRequest(POST, "/").withJsonBody(
          Json.toJson(TestJson("testUser", ""))))
      status(insert) mustBe OK
    }

    "result in BadRequest if json is invalid" in {
      val controller = app.injector.instanceOf[DataController[TestJson]]

      invalidJson()
        .map(jsString => {
          controller.insertData()(
            FakeRequest(POST, "/").withJsonBody(Json.parse(jsString))
          )
        })
        .foreach(result => {
          status(result) mustBe BAD_REQUEST
          contentAsString(result) mustBe "JSON not valid"
        })

    }
    "result in BadRequest if json is missing ignore" ignore {
      val result = app.injector
        .instanceOf[RawDataController]
        .insertData()(
          FakeRequest(POST, "/")
        )
      status(result) mustBe BAD_REQUEST
      contentAsString(result) mustBe "JSON not valid"
    }

    "result in authentication error if token is invalid ignore" ignore {
      val controller = app.injector.instanceOf[RawDataController]

      val result = controller.insertData()(
        FakeRequest(POST, "/").withJsonBody(
          Json.toJson(Validate.RawDataIn(Seq(), Seq(), ""))
        )
      )

      status(result) mustBe BAD_REQUEST
      contentAsString(result) mustBe "Invalid ID token"

    }

    "result in BadRequest if json is missing" in {
      val result = app.injector
        .instanceOf[CompositeRawDataController]
        .insertData()(
          FakeRequest(POST, "/")
        )
      status(result) mustBe BAD_REQUEST
      contentAsString(result) mustBe "JSON not valid"
    }

    "result in authentication error if token is invalid" in {
      val controller = app.injector.instanceOf[CompositeRawDataController]

      val result = controller.insertData()(
        FakeRequest(POST, "/").withJsonBody(
          Json.toJson(Validate.CompRawDataIn(Seq(), ""))
        )
      )

      status(result) mustBe BAD_REQUEST
      contentAsString(result) mustBe "Invalid ID token"

    }

    "should route to the insert data controller correctly" in {
      val request = FakeRequest(POST, "/insertData").withJsonBody(
        Json.toJson(Validate.CompRawDataIn(Seq(), "testUser")))
      val data = route(app, request).get

      status(data) mustBe OK
    }

    "should route to the insert eventData controller correctly" in {
      val request = FakeRequest(POST, "/eventData").withJsonBody(Json.toJson(
        Validate.DataInEvent(DataGenerator.inputEventData(1, 6), "testUser")))
      val data = route(app, request).get

      status(data) mustBe OK
    }

    "should route to the insert annotationData controller correctly" in {
      val request = FakeRequest(POST, "/annotationData").withJsonBody(
        Json.toJson(Validate.DataInEventAnnotation(Seq(), "testUser")))
      val data = route(app, request).get

      status(data) mustBe OK
    }

  }

}
