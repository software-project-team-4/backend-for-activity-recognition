package database

import java.sql.SQLException

import database.Entities.{Accel, Annotation, Event, Gyro}
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Injecting
import utils.{DataGenerator, TestDatabase}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * A test to ensure the database is configured as expected
  */
class DatabaseSpec extends PlaySpec with GuiceOneAppPerSuite with Injecting {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .build()

  val database: Database = app.injector.instanceOf[Database]

  val userRow: Entities.User = DataGenerator.users(1).head

  @deprecated("Old", "1.1-SNAPSHOT")
  val accelRow: Entities.Accel = DataGenerator.datapoints(Seq(userRow), 1).head
  @deprecated("Old", "1.1-SNAPSHOT")
  val gyroRow: Entities.Gyro = Accel.unapply(accelRow).map(Gyro.tupled(_)).get

  val compDataRow: Entities.RawData = DataGenerator.compDatapoints(Seq(userRow), 1).head

  val annotationRow: Entities.Annotation =
    Annotation("testUser", 1, 2, "testEvent")
  val eventRow: Entities.Event = Event("testUser", 1, 2, None, "modelName")

  def await[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  "insert" should {
    "give an error if the row already exists" in {
      await(database.users.insert(userRow))
      intercept[SQLException](await(database.users.insert(userRow)))
      await(database.users.deleteAll())
    }
  }

  "insertOrUpdate" should {
    "not give an error if the row already exists" in {
      await(database.users.insertOrUpdate(userRow))
      await(database.users.insertOrUpdate(userRow))
      await(database.users.deleteAll())
    }
  }

  "insertOrUpdateBatch" should {
    "not give an error if the row already exists" in {
      await(database.users.insert(userRow))
      await(database.users.insertOrUpdateBatch(Seq(userRow)))
      await(database.users.deleteAll())
    }
    "not give an error if the row is duplicated in the set of rows" in {
      await(database.users.insertOrUpdateBatch(Seq(userRow, userRow)))
      await(database.users.deleteAll())
    }
  }

  /*"insert" should {
    "give an error when the user related doesn't exist" in {
      intercept[SQLException](await(database.accels.insert(accelRow)))
      await(database.accels.deleteAll())
      //Intentionally use the accelRow as falls table takes the same datatype
      intercept[SQLException](await(database.falls.insert(accelRow)))
      await(database.falls.deleteAll())
      intercept[SQLException](await(database.gyro.insert(gyroRow)))
      await(database.gyro.deleteAll())
      intercept[SQLException](await(database.events.insert(eventRow)))
      await(database.events.deleteAll())
      intercept[SQLException](await(database.annotations.insert(annotationRow)))
      await(database.annotations.deleteAll())
    }
  }*/

  "insert" should {
    "give an error when the user related doesn't exist" in {
      intercept[SQLException](await(database.compData.insert(compDataRow)))
      await(database.compData.deleteAll())
      //Intentionally use the accelRow as falls table takes the same datatype
      intercept[SQLException](await(database.events.insert(eventRow)))
      await(database.events.deleteAll())
      intercept[SQLException](await(database.annotations.insert(annotationRow)))
      await(database.annotations.deleteAll())
    }
  }
}
