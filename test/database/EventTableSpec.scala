package database

import java.sql.SQLException

import database.Entities.Event
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Injecting
import utils.{DataGenerator, TestDatabase}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * A test to ensure the event database table is configured as expected
  */
class EventTableSpec extends PlaySpec with GuiceOneAppPerSuite with Injecting {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .overrides(bind[Database].to[TestDatabase])
    .build()

  val database: Database = app.injector.instanceOf[Database]

  val userRow: Entities.User = DataGenerator.users(1).head
  val eventRow: Entities.Event = Event(userRow.userId, 1, 2, None, "modelName")

  def await[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  "event table" should {
    "require events to be unique" in {
      await(database.users.insert(userRow))
      await(database.events.insert(eventRow))
      intercept[SQLException](await(database.events.insert(eventRow)))
      await(database.events.deleteAll())
      await(database.users.deleteAll())
    }
  }
}
