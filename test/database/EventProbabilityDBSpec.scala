package database

import java.sql.SQLException

import database.Entities._
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Injecting
import utils.{DataGenerator, TestDatabase}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * A test to ensure the database is configured as expected
  */
class EventProbabilityDBSpec
    extends PlaySpec
    with GuiceOneAppPerSuite
    with Injecting {

  override lazy val app: Application =
    new GuiceApplicationBuilder()
      .overrides(bind[Database].to[TestDatabase])
      .build()

  def database: Database = app.injector.instanceOf[Database]

  val userRow: Entities.User = DataGenerator.users(1).head
  val eventType: Entities.EventType = EventType("test", 1)
  val probabilityRow: Entities.EventProbability =
    EventProbability(1, 1, 1)
  val eventRow: Entities.Event = Event(userRow.userId, 1, 2, None, "modelName")

  def await[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  "insert" should {
    "insert probability when everything is correct" in {
      await(database.users.insert(userRow))
      await(database.events.insert(eventRow))
      await(database.eventTypes.insertOrUpdate(eventType))
      await(database.probabilities.insert(probabilityRow))
      await(database.probabilities.all()).size mustBe 1
      await(database.probabilities.deleteAll())
      await(database.events.deleteAll())
      await(database.eventTypes.deleteAll())
      await(database.users.deleteAll())
    }

    "give an error when the related event row doesn't exist" in {
      await(database.eventTypes.insertOrUpdate(eventType))
      intercept[SQLException](
        await(database.probabilities.insert(probabilityRow)))
      await(database.eventTypes.deleteAll())
    }

    "give an error when the related event type doesn't exist" in {
      await(database.eventTypes.deleteAll())
      await(database.users.insert(userRow))
      await(database.events.insert(eventRow))
      intercept[SQLException](
        await(database.probabilities.insert(probabilityRow)))
      await(database.events.deleteAll())
      await(database.users.deleteAll())
    }
  }
}
