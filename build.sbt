name := """scala-backend"""
organization := "software-project"

version := "1.2-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.8"

libraryDependencies += guice
libraryDependencies += ws
libraryDependencies += "de.leanovate.play-mockws" %% "play-mockws" % "2.7.0" % Test

//Database dependencies
libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.github.tminglei" %% "slick-pg" % "0.17.0"
)

libraryDependencies += "com.google.api-client" % "google-api-client" % "1.27.0"

//Testing dependencies
libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "com.h2database" % "h2" % "1.4.192" % Test,
  jdbc % Test
)

coverageExcludedPackages := "router;controllers.javascript;controllers.ReverseAnnotationController;" +
  "controllers.ReverseEventController;controllers.ReverseRawDataController"
