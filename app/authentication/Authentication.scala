package authentication

import java.util.Collections

import com.google.api.client.googleapis.auth.oauth2.{GoogleIdToken, GoogleIdTokenVerifier}
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import database.Database
import database.Entities.User
import javax.inject.Singleton

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class Authentication {

  /**
    * The client id of the backend for google api
    */
  private val CLIENT_ID = "1085465719221-01su2ht731ug6pnqmekj13h0epi7e8ht.apps.googleusercontent.com"

  private val HTTP_TRANSPORT: NetHttpTransport = GoogleNetHttpTransport.newTrustedTransport()
  private val JSON_FACTORY: JacksonFactory = JacksonFactory.getDefaultInstance

  /**
    * A google id token verifier for verifying the token ids sent by users
    */
  private val verifier = new GoogleIdTokenVerifier.Builder(HTTP_TRANSPORT, JSON_FACTORY)
    .setAudience(Collections.singletonList(CLIENT_ID)).build()

  /**
    * Adds a new user if the user doesn't exist yet
    *
    * @param tokenString the GoogleIdTokenString sent by the user
    * @return userId
    */
  def checkUser(tokenString: String)(implicit database: Database): Option[String] = {
    val token = verifier.verify(tokenString)

    if (token != null) {
      Some(checkUser(token.getPayload))
    } else {
      None
    }
  }

  /**
    * Adds a new user if the user doesn't exist yet
    *
    * @param payload the payload of the GoogleIdToken sent by the user
    * @return userId
    */
  private def checkUser(payload: GoogleIdToken.Payload)(implicit db: Database): String = {
    val userId = payload.getSubject
    import db.dbProfile.api._
    val users = Await.result(db.users.filter(_.userId === userId), Duration.Inf)

    if (users.isEmpty) {
      //If user was not in the database create a new user
      val email = payload.getEmail
      val familyName = payload.get("family_name").toString
      val givenName = payload.get("given_name").toString
      Await.result(db.users.insert(User(userId, givenName, familyName, email)), Duration.Inf)
    }
    userId
  }

}
