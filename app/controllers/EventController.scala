package controllers

import authentication.Authentication
import database.Database
import database.Entities.{Event, EventProbability}
import database.Validate.DataInEvent
import detector.Models
import javax.inject.Inject
import play.api.Logger
import play.api.mvc.ControllerComponents

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Controller for inserting events detected on the device to the database
  *
  */
class EventController @Inject()(implicit db: Database,
                                authentication: Authentication,
                                cc: ControllerComponents)
    extends DataController[DataInEvent]() {

  Models.eventTypes
    .get("sensorflow_1.2")
    .foreach(db.eventTypes.insertOrUpdateBatch)

  /**
    * Insert a batch of data into the database
    *
    * @param userId the user id of the sender
    * @param data   events
    */
  override def handleInsert(userId: String, data: DataInEvent): Unit = {
    val eventData = data.data
    eventData.foreach(event => {
      db.events
        .insertBatchReturnId(
          Seq(
            Event(userId,
                  event.timestamp_start,
                  event.timestamp_start,
                  None,
                  event.model_name + "_lit")))
        .flatMap(
          s =>
            //Only one event
            s.headOption
              .flatMap(id => {
                Models.eventTypes
                  .get(event.model_name)
                  .map(types => {
                    event.probabilities
                      .drop(2)
                      .dropRight(2)
                      .split(",")
                      .map(num => num.toDouble)
                      .zip(types)
                      .map({
                        case (prob, eventType) =>
                          EventProbability(id, eventType.id, prob)
                      })
                  })
              })
              .map(probs => {
                db.probabilities.insertOrUpdateBatch(probs)
              })
              .getOrElse(Future(
                Logger.error(s"Model ${event.model_name} not found!")))
        )
    })
  }
}
