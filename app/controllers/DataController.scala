package controllers

import authentication.Authentication
import database.Validate.DataIn
import database._
import javax.inject._
import play.api.libs.json.{JsError, JsSuccess, OFormat}
import play.api.mvc._

abstract class DataController[T <: DataIn] @Inject()(
    implicit db: Database,
    authentication: Authentication,
    cc: ControllerComponents,
    implicit val format: OFormat[T])
    extends AbstractController(cc) {

  def insertData(): Action[AnyContent] = Action { request =>
    val json = request.body.asJson

    json
      .map(json => {
        json.validate[T] match {
          case s: JsSuccess[T] =>
            val dataIn = s.get
            val userId = authentication.checkUser(dataIn.token).getOrElse("")
            if (userId.isEmpty) {
              BadRequest("Invalid ID token")
            } else {
              handleInsert(userId, dataIn)
              Ok
            }
          case e: JsError =>
            BadRequest("JSON not valid")
        }
      })
      .getOrElse(BadRequest("JSON not valid"))
  }

  /**
    * Handle insert of parsed data
    *
    * @param userId the user sending the data
    * @param data   the parsed data object
    */
  def handleInsert(userId: String, data: T): Unit
}
