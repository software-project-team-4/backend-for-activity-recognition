package controllers

import authentication.Authentication
import database.Database
import database.Validate.RawDataIn
import javax.inject.Inject
import ml._
import play.api.libs.ws._
import play.api.mvc.ControllerComponents
import play.api.{Configuration, Logger}

/**
  * Controller for inserting raw sensor data into the database
  *
  */
@deprecated("Old", "1.1-SNAPSHOT")
class RawDataController @Inject()(implicit db: Database,
                                  implicit val config: Configuration,
                                  implicit val ws: WSClient,
                                  authentication: Authentication,
                                  cc: ControllerComponents)
    extends DataController[RawDataIn]() {

  /**
    * Insert a batch of data into the database
    *
    * @param userId the user id of the sender
    * @param data   sensor readings
    */
  override def handleInsert(userId: String, data: RawDataIn): Unit = {
    val (accel, gyro) = data.toDbFormat(userId)

    db.accels.insertOrUpdateBatch(accel)
    db.gyro.insertOrUpdateBatch(gyro)

    if (accel.nonEmpty) {
      Logger.info(
        accel.size + " accel datapoints inserted for userId: " + userId)
    }
    if (gyro.nonEmpty) {
      Logger.info(gyro.size + " gyro datapoints inserted for userId: " + userId)
    }

    //Insert falls to database
    //FallDetection.processFalls(data.accData, userId)

    val sensorflow = new MlModel(config, db, ws, "sensorflow_1.2")
//    sensorflow.predict(userId, data.accData, data.gyroData)
  }
}
