package controllers

import authentication.Authentication
import database.Database
import database.Validate.CompRawDataIn
import javax.inject.Inject
import ml.MlModel
import play.api.{Configuration, Logger}
import play.api.libs.ws.WSClient
import play.api.mvc.ControllerComponents

/**
  * Controller for inserting raw sensor data into the database
  *
  */
class CompositeRawDataController @Inject()(implicit db: Database,
                                          implicit val config: Configuration,
                                  implicit val ws: WSClient,
                                  authentication: Authentication,
                                  cc: ControllerComponents)
  extends DataController[CompRawDataIn]() {

  /**
    * Insert a batch of data into the database
    *
    * @param userId the user id of the sender
    * @param data   sensor readings from accelerator and gyroscope
    */
  override def handleInsert(userId: String, data: CompRawDataIn): Unit = {
    val dbFormat= data.toDbFormat(userId)

    db.compData.insertOrUpdateBatch(dbFormat)

    if (dbFormat.nonEmpty) {
      Logger.info(
        dbFormat.size + " datapoints inserted for userId: " + userId)
    }

    val sensorflow = new MlModel(config, db, ws, "sensorflow_3.1")
    sensorflow.predict(userId, data.data)
  }
}
