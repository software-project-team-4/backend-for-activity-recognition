package controllers

import authentication.Authentication
import database.Database
import database.Validate.DataInEventAnnotation
import javax.inject.Inject
import play.api.Logger
import play.api.mvc.ControllerComponents

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Controller for inserting raw sensor data into the database
  *
  */
class AnnotationController @Inject()(implicit db: Database,
                                     authentication: Authentication,
                                     cc: ControllerComponents)
    extends DataController[DataInEventAnnotation]() {

  /**
    * Insert a batch of data into the database
    * @param userId the user id of the sender
    * @param data annotation data
    */
  override def handleInsert(userId: String,
                            data: DataInEventAnnotation): Unit = {
    val format = data.toDbFormat(userId)

    Await.result(db.annotations.insertOrUpdateBatch(format), Duration.Inf)

    if (format.nonEmpty) {
      Logger.info(format.size + " annotations inserted for userId: " + userId)
    }
  }
}
