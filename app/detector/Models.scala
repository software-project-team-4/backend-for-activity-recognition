package detector

import database.Entities.EventType

object Models {

  type ModelName = String

  val eventTypes: Map[ModelName, Seq[EventType]] = Map(
    "sensorflow_1.2" -> Seq("stand",
                            "sit",
                            "walk",
                            "stairsup",
                            "stairsdown",
                            "bike").zipWithIndex.map({
      case (name, id) => EventType(name, id)
    }),
    "sensorflow_2.6" -> Seq("stand",
                            "sit",
                            "walk",
                            "stairsup",
                            "stairsdown",
                            "bike").zipWithIndex.map({
      case (name, id) => EventType(name, id)
    }),
    "sensorflow_3.1" -> Seq("stand",
                            "sit",
                            "walk").zipWithIndex.map({
      case (name, id) => EventType(name, id)
    })
  )

  val inputLengths: Map[ModelName, Int] = Map(
    "sensorflow_1.2" -> 250,
    "sensorflow_2.6" -> 250,
    "sensorflow_3.1" -> 250
  )

  val outputLengths: Map[ModelName, Int] = Map(
    "sensorflow_1.2" -> 6,
    "sensorflow_2.6" -> 6,
    "sensorflow_3.1" -> 3
  )

  val urls: Map[ModelName, String] = Map(
    "sensorflow_1.2" -> ":8501/v1/models/sensorflow:predict",
    "sensorflow_2.6" -> ":8501/v1/models/sensorflow:predict",
    "sensorflow_3.1" -> ":8501/v1/models/sensorflow:predict"
  )

}
