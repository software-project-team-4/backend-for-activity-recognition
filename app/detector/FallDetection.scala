package detector

import database.Database
import database.Entities.Accel
import database.Validate.Datapoint
import play.api.Logger

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration.Duration

@deprecated("Old", "1.1-SNAPSHOT")
object FallDetection {

  /**
    * The maximum length of the acceleration vector for the phone to be considered in free fall
    */
  private val fallThreshold = 1.0

  /**
    * Iterate over given sequence of [[database.Validate.Datapoint]] and store representing a free fall to database
    *
    * @param data a set of accelerator sensor data
    */
  def processFalls(data: Seq[Datapoint], userId: String)(
      implicit db: Database): Unit = {
    val falls = findFalls(data)

    if (falls.nonEmpty) {
      Logger.info(s"Inserting ${falls.size} falls to database")
      Await.ready(db.falls.insertOrUpdateBatch(findFalls(data).map(a =>
                    Accel(userId, a.timestamp, a.x, a.y, a.z))),
                  Duration.Inf)
    }

  }

  /**
    * Find the data points that specify a fall
    *
    * @param data a sequence of [[database.Validate.Datapoint]]
    * @return a sequence of datapoints at which the sensor was found to be in free fall
    */
  def findFalls(data: Seq[Datapoint]): mutable.Seq[Datapoint] = {
    val result = mutable.Buffer[Datapoint]()

    for (p <- data) {
      val (_, x, y, z) = (p.timestamp, p.x, p.y, p.z)
      val length = Math.sqrt(x * x + y * y + z * z)
      if (length < fallThreshold) {
        result += p
      }
    }
    result
  }

}
