package ml

import database.Database
import database.Entities._
import database.Validate.RawDatapoint
import detector.Models
import javax.inject.Inject
import play.api.{Configuration, Logger}
import play.api.libs.json.{JsError, JsObject, JsSuccess, Json}
import play.api.libs.ws._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class MlModel @Inject()(config: Configuration,
                        db: Database,
                        ws: WSClient,
                        modelName: String) {

  val ip = s"http://${config.get[String]("serving.ip")}"

  val inputLength: Int = Models.inputLengths(modelName)
  val outputLength: Int = Models.outputLengths(modelName)

  val url: String = Models.urls(modelName)
  val modelUrl: String = ip + url

  val eventInserts = Models.eventTypes(modelName)

  Await.result(db.eventTypes.insertOrUpdateBatch(eventInserts), Duration.Inf)

  /**
    * Takes a sequence of Datapoints and groups them into groups of size inputLength
    * and converts datapoints into Vectors with only sensory readings incl. as
    * Doubles. Discards groups with less elements than inputLength.
    *
    * Vectors are then easily converted into Json.
    *
    * @param data a seq of datapoints
    * @return A tuple with starting and ending timestamps and a batch of features.
    * A feature is a Vector of length inputLength with Vectors as its elements
    * containing raw sensor readings.
    */
  def toBatch(userId: String, data: Seq[RawDatapoint])
    : (Vector[Event], JsObject) = {

    val batch =
      data
      .grouped(inputLength)
      .filter(group => group.size == inputLength)
      .toVector.map(_.toVector)

    val features =
      batch.map{ group =>
        group.map{ case (datapoint: RawDatapoint) =>
          Vector(datapoint.acc_x, datapoint.acc_y, datapoint.acc_z,
            datapoint.gyr_x, datapoint.gyr_y, datapoint.gyr_z)
        }
      }

    val events =
      batch.map{ group =>
        val start = group.map{case (datapoint: RawDatapoint) =>
          datapoint.timestamp
        }.min

        val end = group.map{case (datapoint: RawDatapoint) =>
          datapoint.timestamp
        }.max

        Event(userId, start, end, None, modelName)
      }

    val json = Json.obj("instances" -> Json.toJson(features))

    (events, json)
  }

  def predict(userId: String, data: Seq[RawDatapoint]): Unit = {

    val (events, json) = toBatch(userId, data)
    val eventInsert = db.events.insertBatchReturnId(events)

    ws.url(modelUrl).post(json).flatMap{ response =>
      if(response.status == 200){

        (response.json \ "predictions").validate[Seq[Seq[Double]]] match {
          case s: JsSuccess[Seq[Seq[Double]]] =>
            val predSeq = s.get
            Logger.info(predSeq.size + " predictions received")

            eventInsert.flatMap{ foreignKeys => Future {
              Logger.info(foreignKeys.size + " events inserted")
              predSeq.zip(foreignKeys)
              .flatMap{ case (prob: Seq[Double], eventFk: Int) =>
                  prob.zipWithIndex.map{ case (prob: Double, i: Int) =>
                    EventProbability(eventFk, i, prob)
                  }
              }
            }}.flatMap{ eventProbs =>
              db.probabilities.insertOrUpdateBatch(eventProbs)
            }.flatMap{ t =>
              Future{ Logger.info("probabilities inserted") }
            }


          case e: JsError =>
            Future{ Logger.error("Prediction failed") }
        }

      } else {
        Future{ Logger.error(response.toString) }
      }
    }

  }

}
