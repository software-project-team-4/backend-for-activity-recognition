package database

import com.github.tminglei.slickpg._
import slick.basic.Capability
import slick.jdbc.JdbcCapabilities

/**
  * Adds support for upsert with postgres via slick-pg
  * [[https://github.com/tminglei/slick-pg]]
  */
trait CustomPostgresDriver
    extends ExPostgresProfile
    with PgArraySupport
    with PgDate2Support
    with PgNetSupport
    with PgLTreeSupport
    with PgRangeSupport
    with PgHStoreSupport
    with PgSearchSupport {

  override val api: API
    with ArrayImplicits
    with DateTimeImplicits
    with NetImplicits
    with LTreeImplicits
    with RangeImplicits
    with HStoreImplicits
    with SearchImplicits
    with SearchAssistants = new API with ArrayImplicits with DateTimeImplicits
  with NetImplicits with LTreeImplicits with RangeImplicits with HStoreImplicits
  with SearchImplicits with SearchAssistants {}

  override protected def computeCapabilities: Set[Capability] =
    super.computeCapabilities + JdbcCapabilities.insertOrUpdate

}

object CustomPostgresDriver extends CustomPostgresDriver
