package database

import database.Entities.{Accel, Annotation, Gyro, RawData}
import play.api.libs.json.{JsValue, Json, OFormat}

/**
  * A helper class used for validating and parsing the shape of received json data
  */
object Validate {

  implicit val gyroFormat: OFormat[Gyro] = Json.format[Gyro]
  implicit val dataFormat: OFormat[Datapoint] = Json.format[Datapoint]
  implicit val acceleratorFormat: OFormat[Accel] = Json.format[Accel]
  implicit val rawDataFormat: OFormat[RawDatapoint] = Json.format[RawDatapoint]
  implicit val compData: OFormat[CompRawDataIn] = Json.format[CompRawDataIn]
  implicit val compFormat: OFormat[RawData] = Json.format[RawData]
  implicit val rawData: OFormat[RawDataIn] = Json.format[RawDataIn]
  implicit val eventData: OFormat[EventData] = Json.format[EventData]
  implicit val annotationData: OFormat[AnnotationData] = Json.format[AnnotationData]
  implicit val eventDataInFormat: OFormat[DataInEventAnnotation] = Json.format[DataInEventAnnotation]
  implicit val eventDataFormat: OFormat[DataInEvent] = Json.format[DataInEvent]

  /**
    * Represents a sensor datapoint received
    * @param timestamp the time at which the data was gathered
    * @param x the x axis reading
    * @param y the y axis reading
    * @param z the z axis reading
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  case class Datapoint(
      timestamp: Long,
      x: Double,
      y: Double,
      z: Double
  )
  /**
    * Represents a sensor datapoint received
    *
    * @param timestamp the time at which the data was gathered
    * @param acc_x      the accelerator's x axis reading
    * @param acc_y      the accelerator's y axis reading
    * @param acc_z      the accelerator's z axis reading
    * @param gyr_x      the gyro's x axis reading
    * @param gyr_y      the gyro's y axis reading
    * @param gyr_z      the gyro's z axis reading
    */
  case class RawDatapoint(
                           timestamp: Long,
                           acc_x: Double,
                           acc_y: Double,
                           acc_z: Double,
                           gyr_x: Double,
                           gyr_y: Double,
                           gyr_z: Double
                         )

  /**
    * Represents a annotation data received
    * @param timestamp_start start timestamp of the event
    * @param timestamp_end end timestamp of the event
    * @param event_name name of the event type
    */
  case class AnnotationData(
      timestamp_start: Long,
      timestamp_end: Long,
      event_name: String
  )

  /**
    * Represents the data of an event received from the device
    * @param timestamp_start start timestamp of the event
    * @param timestamp_end end timestamp of the event
    * @param probabilities the probabilities of actions described by the model
    * @param model_name the name of the model used for the recognition
    */
  case class EventData(
                        timestamp_start: Long,
                        timestamp_end: Long,
                        probabilities: String,
                        model_name: String
  )

  /**
    * Any authenticated data that gets sent to the server should have an id token attached to it
    */
  trait DataIn {
    def token: String
  }

  @deprecated("Old", "1.1-SNAPSHOT")
  case class RawDataIn(accData: Seq[Datapoint],
                       gyroData: Seq[Datapoint],
                       token: String)
      extends DataIn {

    /**
      * Converts the input data into database entities
      * @param userId the user id that the data came from
      * @return a pair of lists that contain the database entities defined by the given data
      */
    def toDbFormat(userId: String): (Seq[Accel], Seq[Gyro]) = {
      (accData.map(a => Accel(userId, a.timestamp, a.x, a.y, a.z)),
       gyroData.map(g => Gyro(userId, g.timestamp, g.x, g.y, g.z)))
    }
  }

  case class CompRawDataIn(data: Seq[RawDatapoint], token: String) extends DataIn {

    /**
      * Converts the input data into database entities
      * @param userId the user id that the data came from
      * @return a list that contain the database entities defined by the given data
      */
    def toDbFormat(userId: String): Seq[RawData] = {
      data.map(a => RawData(userId, a.timestamp, a.acc_x, a.acc_y, a.acc_z, a.gyr_x, a.gyr_y, a.gyr_z))
    }
  }

  case class DataInEventAnnotation(
      data: Seq[AnnotationData],
      token: String
  ) extends DataIn {

    /**
      * Converts the input data into database entities
      * @param userId the user id that the data came from
      * @return a pair of lists that contain the database entities defined by the given data
      */
    def toDbFormat(userId: String): Seq[Annotation] = {
      data.map(a =>
        Annotation(userId, a.timestamp_start, a.timestamp_end, a.event_name))
    }
  }

  /**
    * Defines the input format of event data
    * @param data a list of events detected
    * @param token the authentication token
    */
  case class DataInEvent(data: Seq[EventData], token: String) extends DataIn

  def toJson(datapoints: Seq[Accel]): JsValue = Json.toJson(datapoints)

  def toJsonComp(datapoints: Seq[RawData]): JsValue = Json.toJson(datapoints)

}
