package database

import database.Database.BaseEntity
import database.Entities.Accel
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.jdbc.meta.MTable
import slick.lifted._

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class Database @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(
    implicit executionContext: ExecutionContext)
    extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  /**
    * Gives access to the profile in use from outside the class
    */
  val dbProfile = profile

  /**
    * Find the tables that exist in the database
    */
  private def existingTables: Set[String] =
    Await.result(
      db.run(MTable.getTables).map(names => names.map(_.name.name).toSet),
      Duration.Inf)

  /**
    * List of repositories using the database
    */
  private val repositories: mutable.Buffer[BaseRepository[_, _]] =
    mutable.Buffer()

  /**
    * Create all the repositories
    */
  def createTables(): Unit = {
    val existing = existingTables
    repositories
      .filter(repo => !existing.contains(repo.tableName))
      .foreach(_.create())
  }

  /**
    * Defines all the functions used to edit a database table
    * @param query the table query that the functions will be executed on
    * @tparam T type of the entity stored in the table related to this
    * @tparam E type of the table
    */
  class BaseRepository[T <: BaseEntity, E <: BaseTable[T]](
      val query: TableQuery[E]) {

    //Add this repo to the list of repos for operations on all repos
    repositories += this

    /**
      * Name of the table used by this repo
      */
    val tableName: String = query.baseTableRow.tableName

    /**
      * Create the table used by this repository
      */
    def create(): Unit = {
      Await.ready(db.run(query.schema.create), Duration.Inf)
    }

    /**
      * Get all the rows in the database
      * @return a future that completes when the query is done
      */
    def all(): Future[Seq[T]] = {
      db.run(query.result)
    }

    /**
      * Deletes all rows from the database
      * @return a future that completes when the deletion finishes
      */
    def deleteAll(): Future[Int] = {
      db.run(query.delete)
    }

    /**
      * Insert a row of data
      *
      * Fails if a row with the same primary key already exists
      *
      * @param row the row
      * @return a future that finishes when the row has been inserted (or the insert has failed)
      */
    def insert(row: T): Future[Int] = {
      db.run(query += row)
    }

    /**
      * Insert or update a row
      *
      * If a row already exists with the same primary key the row will be updated to match given row
      * Otherwise a new row will be inserted
      *
      * @param row the row
      * @return a future that finishes when the row has been inserted or updated
      */
    def insertOrUpdate(row: T): Future[Int] = {
      db.run(query.insertOrUpdate(row))
    }

    /**
      * Insert or update a set of data
      *
      * If a row already exists with the same primary key the row will be updated to match given row
      * Otherwise a new row will be inserted
      *
      * @param rows the rows to be inserted
      * @return a future that finishes when the rows have been inserted or updated
      */
    def insertOrUpdateBatch(rows: Seq[T]): Future[_] = {
      val toInsert = rows.map(query.insertOrUpdate(_))
      val inSeq = DBIO.sequence(toInsert)
      db.run(inSeq)
    }

    /**
      * Filter the database with given condition
      * @param expr the expression that the rows will be matched against
      * @return a future that results in a seq of rows that match the expression
      */
    def filter[C <: Rep[_]](expr: E => C)(
        implicit wt: CanBeQueryCondition[C]): Future[Seq[T]] = {
      db.run(query.filter(expr).result)
    }

  }

  /**
    * A super class for all the database tables
    * @param tableTag a tag for the table
    * @param tableName the name of the table in the database
    * @tparam E the type of the entity that will be stored in this table
    */
  abstract class BaseTable[E <: BaseEntity](tableTag: Tag, tableName: String)
      extends Table[E](tableTag, None, tableName)

  /**
    * Defines the table for accelerometer data
    * @param tag a tag for the table
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  class AccelTable(tag: Tag)
      extends BaseTable[Entities.Accel](tag, "accel_data") {

    def userId: Rep[String] = column[String]("user_id")
    def timestamp: Rep[Long] = column[Long]("timestamp")
    def accelX: Rep[Double] = column[Double]("accel_x")
    def accelY: Rep[Double] = column[Double]("accel_y")
    def accelZ: Rep[Double] = column[Double]("accel_z")

    def pk: PrimaryKey = primaryKey("pk_accel", (userId, timestamp))

    //Blocks adding datapoints that aren't related to an user
    def userFk: ForeignKeyQuery[UserTable, Entities.User] =
      foreignKey("user_fk_accel", userId, users.query)(_.userId)

    //Scalastyle disabled because the * function is required by the database library
    override def * = // scalastyle:ignore
      (userId, timestamp, accelX, accelY, accelZ) <> (Accel.tupled, Accel.unapply)

  }

  /**
    * Contains the event types that can be recognized by the ML models
    */
  class EventTypeTable(tag: Tag)
      extends BaseTable[Entities.EventType](tag, "event_type") {
    def id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    def name: Rep[String] = column[String]("name")

    //Scalastyle disabled because the * function is required by the database library
    def * = //scalastyle:ignore
      (name, id) <> (Entities.EventType.tupled, Entities.EventType.unapply)
  }

  /**
    * Contains the probabilities of event types found by the ML models
    */
  class EventProbabilityTable(tag: Tag)
      extends BaseTable[Entities.EventProbability](tag, "probability_data") {

    def eventId: Rep[Int] = column[Int]("event_id")
    def eventType: Rep[Int] = column[Int]("event_type")
    def probability: Rep[Double] = column[Double]("probability")

    def pk: PrimaryKey =
      primaryKey("pk_event_probability", (eventId, eventType))

    def eventFK: ForeignKeyQuery[EventTable, Entities.Event] =
      foreignKey("event_fk_probability", eventId, events.query)(_.eventId)

    def eventTypeFK: ForeignKeyQuery[EventTypeTable, Entities.EventType] =
      foreignKey("event_type_fk_probability", eventType, eventTypes.query)(_.id)

    //Scalastyle disabled because the * function is required by the database library
    def * = //scalastyle:ignore
      (eventId, eventType, probability) <> (Entities.EventProbability.tupled, Entities.EventProbability.unapply)
  }

  /**
    * Defines the table for gyroscope data
    * @param tag a tag for the table
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  class GyroTable(tag: Tag) extends BaseTable[Entities.Gyro](tag, "gyro_data") {
    def userId: Rep[String] = column[String]("user_id")
    def timestamp: Rep[Long] = column[Long]("timestamp")
    def gyroX: Rep[Double] = column[Double]("gyro_x")
    def gyroY: Rep[Double] = column[Double]("gyro_y")
    def gyroZ: Rep[Double] = column[Double]("gyro_z")

    def pk: PrimaryKey = primaryKey("pk_gyro", (userId, timestamp))

    //Blocks adding datapoints that aren't related to an user
    def userFk: ForeignKeyQuery[UserTable, Entities.User] =
      foreignKey("user_fk_gyro", userId, users.query)(_.userId)

    //Scalastyle disabled because the * function is required by the database library
    def * = //scalastyle:ignore
      (userId, timestamp, gyroX, gyroY, gyroZ) <> (Entities.Gyro.tupled, Entities.Gyro.unapply)
  }

  @deprecated("Old", "1.1-SNAPSHOT")
  class FallsTable(tag: Tag)
      extends BaseTable[Entities.Accel](tag, "fall_data") {
    def userId: Rep[String] = column[String]("user_id")
    def timestamp: Rep[Long] = column[Long]("timestamp")
    def accelX: Rep[Double] = column[Double]("accel_x")
    def accelY: Rep[Double] = column[Double]("accel_y")
    def accelZ: Rep[Double] = column[Double]("accel_z")

    def pk: PrimaryKey = primaryKey("pk_falls", (userId, timestamp))

    //Blocks adding datapoints that aren't related to an user
    def userFk: ForeignKeyQuery[UserTable, Entities.User] =
      foreignKey("user_fk_falls", userId, users.query)(_.userId)

    //Scalastyle disabled because the * function is required by the database library
    def * : ProvenShape[Entities.Accel] = //scalastyle:ignore
      (userId, timestamp, accelX, accelY, accelZ) <> (Entities.Accel.tupled, Entities.Accel.unapply)
  }

  class EventTable(tag: Tag) extends BaseTable[Entities.Event](tag, "events") {
    def eventId: Rep[Int] = column[Int]("event_id", O.PrimaryKey, O.AutoInc)
    def userId: Rep[String] = column[String]("user_id")
    def startTimestamp: Rep[Long] = column[Long]("timestamp_start")
    def endTimestamp: Rep[Long] = column[Long]("timestamp_end")
    def modelName: Rep[String] = column[String]("model_name")

    //Blocks adding datapoints that aren't related to an user
    def userFk: ForeignKeyQuery[UserTable, Entities.User] =
      foreignKey("user_fk_events", userId, users.query)(_.userId)

    def uniqueIndex: Index =
      index("uniqueIndexEventTable",
            (userId, startTimestamp, endTimestamp),
            unique = true)

    def * : ProvenShape[Entities.Event] = //scalastyle:ignore
      (userId, startTimestamp, endTimestamp, eventId.?, modelName) <> (Entities.Event.tupled, Entities.Event.unapply)

  }

  class AnnotationTable(tag: Tag)
      extends BaseTable[Entities.Annotation](tag, "annotations") {
    def userId: Rep[String] = column[String]("user_id")
    def startTimestamp: Rep[Long] = column[Long]("timestamp_start")
    def endTimestamp: Rep[Long] = column[Long]("timestamp_end")
    def eventName: Rep[String] = column[String]("event_name")

    def pk: PrimaryKey = primaryKey("pk_annotations", (userId, startTimestamp))

    //Blocks adding datapoints that aren't related to an user
    def userFk: ForeignKeyQuery[UserTable, Entities.User] =
      foreignKey("user_fk_annotations", userId, users.query)(_.userId)

    def * : ProvenShape[Entities.Annotation] = //scalastyle:ignore
      (userId, startTimestamp, endTimestamp, eventName) <> (Entities.Annotation.tupled, Entities.Annotation.unapply)

  }

  /**
    * Defines the table for the user data
    * @param tag a tag for the table
    */
  class UserTable(tag: Tag) extends BaseTable[Entities.User](tag, "users") {
    def userId: Rep[String] = column[String]("user_id", O.PrimaryKey)
    def firstName: Rep[String] = column[String]("first_name")
    def lastName: Rep[String] = column[String]("last_name")
    def email: Rep[String] = column[String]("email")

    //Scalastyle disabled because the * function is required by the database library
    def * = //scalastyle:ignore
      (userId, firstName, lastName, email) <> (Entities.User.tupled, Entities.User.unapply)
  }

  /**
    * Defines the table for the sensordata
    * @param tag a tag for the table
    */
  class CompDataTable(tag: Tag) extends BaseTable[Entities.RawData](tag, "comp_data") {
    def userId: Rep[String] = column[String]("user_id")
    def timestamp: Rep[Long] = column[Long]("timestamp")
    def accelX: Rep[Double] = column[Double]("accel_x")
    def accelY: Rep[Double] = column[Double]("accel_y")
    def accelZ: Rep[Double] = column[Double]("accel_z")
    def gyroX: Rep[Double] = column[Double]("gyro_x")
    def gyroY: Rep[Double] = column[Double]("gyro_y")
    def gyroZ: Rep[Double] = column[Double]("gyro_z")

    def pk: PrimaryKey = primaryKey("pk_comp", (userId, timestamp))

    def userFk: ForeignKeyQuery[UserTable, Entities.User] =
      foreignKey("user_fk_comp", userId, users.query)(_.userId)

    //Scalastyle disabled because the * function is required by the database library
    def * = //scalastyle:ignore
      (userId, timestamp, accelX, accelY, accelZ, gyroX, gyroY, gyroZ) <> (Entities.RawData.tupled, Entities.RawData.unapply)
  }

  //User repository
  val users: BaseRepository[Entities.User, UserTable] =
    new BaseRepository[Entities.User, UserTable](TableQuery[UserTable])

  //CompositeData repository
  val compData: BaseRepository[Entities.RawData, CompDataTable] =
    new BaseRepository[Entities.RawData, CompDataTable](
      TableQuery[CompDataTable]
    )

  //Event types repository
  val eventTypes: BaseRepository[Entities.EventType, EventTypeTable] =
    new BaseRepository[Entities.EventType, EventTypeTable](
      TableQuery[EventTypeTable])

  //Accelerometer datapoints
  @deprecated("Old", "1.1-SNAPSHOT")
  val accels =
    new BaseRepository[Entities.Accel, AccelTable](TableQuery[AccelTable])
  //Gyroscope datapoints
  @deprecated("Old", "1.1-SNAPSHOT")
  val gyro =
    new BaseRepository[Entities.Gyro, GyroTable](TableQuery[GyroTable])

  //Falls repository
  @deprecated("Old", "1.1-SNAPSHOT")
  val falls =
    new BaseRepository[Entities.Accel, FallsTable](TableQuery[FallsTable])


  class EventRepo extends BaseRepository[Entities.Event, EventTable](
    TableQuery[EventTable]){

    def insertBatchReturnId(events: Seq[Entities.Event]): Future[Seq[Int]] = {
      db.run((query returning query.map(_.eventId)) ++= events)
    }
  }
  //Events repository
  val events =
    new EventRepo

  //Annotation repository
  val annotations =
    new BaseRepository[Entities.Annotation, AnnotationTable](
      TableQuery[AnnotationTable])

  //Event probabilities repository
  val probabilities
    : BaseRepository[Entities.EventProbability, EventProbabilityTable] =
    new BaseRepository[Entities.EventProbability, EventProbabilityTable](
      TableQuery[EventProbabilityTable])

  import database.Database.autoCreateTables
  if (autoCreateTables) {
    createTables()
  }

}

object Database {

  /**
    * An entity that may be stored in the database
    */
  trait BaseEntity

  /**
    * Whether or not the tables should be created automatically when the repositories are created
    */
  var autoCreateTables = true

}
