package database

import database.Database.BaseEntity

/**
  * Contains the models used for the database data
  */
object Entities {

  /**
    * A type of event detectable by a ML model
    * @param id pk id pf the event
    * @param name string representation of the event
    */
  case class EventType(
      name: String,
      id: Int
  ) extends BaseEntity

  /**
    * The probability of an event type for a given event
    *
    * @param eventId        the event that this probability is for
    * @param eventType      the type of the event
    * @param probability    the probability of the given event being of the event type
    */
  case class EventProbability(
      eventId: Int,
      eventType: Int,
      probability: Double
  ) extends BaseEntity

  /**
    * Accelerometer datapoint
    *
    * @param userId    the user id of the user who sent this data
    * @param timestamp the time that the data was gathered at
    * @param accelX    x axis reading of the accelerometer
    * @param accelY    y axis reading of the accelerometer
    * @param accelZ    z axis reading of the accelerometer
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  case class Accel(
      userId: String,
      timestamp: Long,
      accelX: Double,
      accelY: Double,
      accelZ: Double
  ) extends BaseEntity

  /**
    * Gyroscope datapoint
    *
    * @param userId    the user id of  the user who sent this data
    * @param timestamp the time that the data was gathered at
    * @param gyroX     the x axis reading of the gyroscope
    * @param gyroY     the y axis reading of the gyroscope
    * @param gyroZ     the z axis reading of the gyroscope
    */
  @deprecated("Old", "1.1-SNAPSHOT")
  case class Gyro(
      userId: String,
      timestamp: Long,
      gyroX: Double,
      gyroY: Double,
      gyroZ: Double
  ) extends BaseEntity

  /**
    * Accelerometer and gyroscope datapoints
    *
    * @param userId    the user id of the user who sent this data
    * @param timestamp the time that the data was gathered at
    * @param accX      x axis reading of the accelerometer
    * @param accY      y axis reading of the accelerometer
    * @param accZ      z axis reading of the accelerometer
    * @param gyrX      x axis reading of the gyroscope
    * @param gyrY      y axis reading of the gyroscope
    * @param gyrZ      z axis reading of the gyroscope
    */
  case class RawData(
                      userId: String,
                      timestamp: Long,
                      accX: Double,
                      accY: Double,
                      accZ: Double,
                      gyrX: Double,
                      gyrY: Double,
                      gyrZ: Double
                    ) extends BaseEntity

  /**
    * Event database entity
    *
    * @param userId         the id of the user who sent this data
    * @param startTimestamp the starting timestamp of this event
    * @param endTimestamp   the ending timestamp of this event
    * @param modelName      the name of the model which predicted the event
    */
  case class Event(
      userId: String,
      startTimestamp: Long,
      endTimestamp: Long,
      id: Option[Int] = None,
      modelName: String
  ) extends BaseEntity

  /**
    * Annotation database entity
    *
    * @param userId         the id of the user who sent this data
    * @param startTimestamp the starting timestamp of this event
    * @param endTimestamp   the ending timestamp of this event
    * @param eventName      the name/type of the event
    */
  case class Annotation(
      userId: String,
      startTimestamp: Long,
      endTimestamp: Long,
      eventName: String
  ) extends BaseEntity

  /**
    * Represents an user in the database
    *
    * @param userId the id of the user
    */
  case class User(
      userId: String,
      firstName: String,
      lastName: String,
      email: String
  ) extends BaseEntity

}
