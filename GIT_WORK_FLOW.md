# Android for activity monitoring

## Git workflow

![Git branching model](https://gitlab.com/software-project-team-4/android-for-activity-recognition/raw/master/images/nice_branching.png)

### Git naming for branches

feature: feature_(add name here)<br/>
release: release_(add_version_here)<br/>
hotfix:  hotfix_(add_name_here)<br/>

### Possible merges

- develop -> feature (For updating changes to the branch)
- feature -> develop
- hotfix  -> master
- hotfix  -> development
- master  -> hotfix (For updating changes to the branch)
- release -> develop
- release -> master

### Git workflow when creating a new branch for a feature/release

- git checkout develop (go to development branch)
- git pull -r (pull latest changes)
- git branch branch_name (Create a new branch for the feature. Replace branch_name with your own name)
- git add -A (saving changes locally)
- git commit -m "write a commit message here" (saving changes locally)
- git push origin branch_name (Add branch to gitlab only if someone else needs to work with the same feature)

### Save changes locally

- git add file_name
- git commit -m "write a commit message here"

### Merge feature branch to the development branch

- git checkout develop (go to development branch)
- git pull -r (pull latest changes)
- git merge --no-ff feature_name (merge feature branch to development branch, solve merge conflicts)
